How to proceed?
1. Use rolling() to calculate:
 - Max over the last 28 days
 -min over the last 28 days
2. Boolean indexing:
 - If 'close' > max28 then Buy = 1
 - If 'close' < max28 then Buy = -1

3. Plot the results

Hint: Add two new columns 'Buy' and 'Sell'